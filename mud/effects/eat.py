# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .effect import Effect2
from mud.events import EatEvent

class EatEffect(Effect2):
    EVENT = EatEvent
