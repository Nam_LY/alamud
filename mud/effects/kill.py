# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import KillEvent, KillWithEvent

class KillEffect(Effect2):
    EVENT = KillEvent

class KillWithEffect(Effect3):
    EVENT = KillWithEvent