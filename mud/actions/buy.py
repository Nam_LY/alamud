# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .action import Action2
from mud.events import BuyEvent


class BuyAction(Action2):
    EVENT = BuyEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "buy"
