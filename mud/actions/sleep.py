# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .action import Action2
from mud.events import SleepEvent

class SleepAction(Action2):
    EVENT = SleepEvent
    ACTION = "sleep"
    RESOLVE_OBJECT = "resolve_for_operate"
