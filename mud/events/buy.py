# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .event import Event2

class BuyEvent(Event2):
    NAME = "buy"

    def perform(self):
        if not self.object.has_prop("buyable"):
            self.fail()
            return self.inform("buy.failed")
        self.inform("buy")
