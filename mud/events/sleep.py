# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .event import Event2

class SleepEvent(Event2):
    NAME = "sleep"

    def perform(self):
        if not self.object.has_prop("sleepable"):
            self.fail()
            return self.inform("sleep.failed")
        self.inform("sleep")
