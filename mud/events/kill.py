# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .event import Event2, Event3

class KillEvent(Event2):
    NAME = "kill"

    def perform(self):
        self.inform("kill")


class KillWithEvent(Event3):
    NAME = "kill-with"

    def perform(self):
        self.inform("kill-with")